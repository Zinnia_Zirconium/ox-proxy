#!/bin/bash
temp="$(mktemp -d)" || exit
cp -pv youtube-dl "$temp"
pushd "$temp"
unzip youtube-dl
echo 'from .youtube import YoutubeIE' > youtube_dl/extractor/extractors.py
sed -i 's/GenericIE/YoutubeIE/g' youtube_dl/extractor/__init__.py
find -name '*.py' | xargs sed -i 's/^import re\b/import regex as re/'
python -OO -m compileall .
zip -0 youtube-dl.zip $( find -name '*.pyo' )
popd
cp -pv "${temp}/youtube-dl.zip" .
