#!/bin/bash
temp="$(mktemp -d)" || exit
cp -pv youtubedl-lazy "$temp"
pushd "$temp"
unzip youtubedl-lazy
echo -e '\n\n_ALL_CLASSES = [YoutubeIE]\n' >> youtube_dl/extractor/lazy_extractors.py
find -name '*.py' | xargs sed -i 's/^import re\b/import regex as re/'
python -OO -m compileall .
zip -0 youtubedl-lazy.zip $( find -name '*.pyo' )
popd
cp -pv "${temp}/youtubedl-lazy.zip" .
