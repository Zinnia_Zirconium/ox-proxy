#!/bin/bash
unset -v DIAGNOSTICS ; # DIAGNOSTICS="-L debug" 
unset -v TRACING ; # TRACING="strace -f -tt" 
unset -v QUIC_OPTIONS ; QUIC_OPTIONS="-g -o cc_algo=1 -o idle_timeout=600 -o mtu_probe_timer=15000 -o noprogress_timeout=1800 -o pace_packets=0 -o send_prst=1" 
unset -v AUTHORITY ; AUTHORITY="ox-proxy.gq www.ox-proxy.gq ox-proxy.no-ip.org" 
unset -v ADDRESS ; ADDRESS=:: 
unset -v PORT ; PORT=443 
[[ "${1}" =~ ^[1-9][0-9]{0,4}$ ]] && [[ "${1}" -gt 0 && "${1}" -le 0xffff ]] && PORT="${1}" 
unset -v CHAIN ; CHAIN=/home/ubuntu/.acme.sh/ox-proxy.gq/fullchain.cer 
unset -v KEY ; KEY=/home/ubuntu/.acme.sh/ox-proxy.gq/ox-proxy.gq.key 
unset -v OX ; OX=./ox-proxy.sh 
unset -v CERTIFICATE_OPTIONS ; for A in $AUTHORITY ; do CERTIFICATE_OPTIONS+="-c ${A},${CHAIN},${KEY} " ; done 
( dirname_re="(^.*/)[^/]+$" ; [[ "${BASH_SOURCE[0]}" =~ $dirname_re ]] && cd "${BASH_REMATCH[1]}" ; ${TRACING} lsquic/http_server-popen ${DIAGNOSTICS} ${QUIC_OPTIONS} -s ${ADDRESS}:${PORT} ${CERTIFICATE_OPTIONS} -e ${OX} ) 
exit 0
