<?php
if (preg_match('/^\/[.cjn]/', $_SERVER["REQUEST_URI"])) {
ob_start();
ob_start("ob_gzhandler");
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "http://127.0.0.1:8080" . $_SERVER["REQUEST_URI"]);
curl_setopt($curl, CURLOPT_HEADER, 0);
curl_exec($curl);
curl_close($curl);
if (ob_get_length() == 0) {
ob_end_clean();
http_response_code(404);
} else {
ob_end_flush();
}
header("Content-Length: " . ob_get_length());
header("Content-Type: text/plain; charset=UTF-8");
ob_end_flush();
exit;
}
if (preg_match('/^\/\./', $_SERVER["REQUEST_URI"])) {
return false;
} else {
header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"], true, 301);
}
?>
