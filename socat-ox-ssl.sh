#!/bin/bash
unset -v DIAGNOSTICS ; # DIAGNOSTICS="-d -d" 
unset -v PORT ; PORT=443 
unset -v CERT ; CERT=/home/ubuntu/.acme.sh/ox-proxy.gq/ox-proxy.gq.cer 
unset -v KEY ; KEY=/home/ubuntu/.acme.sh/ox-proxy.gq/ox-proxy.gq.key 
unset -v CA ; CA=/home/ubuntu/.acme.sh/ox-proxy.gq/ca.cer 
unset -v DEFER_ACCEPT ; DEFER_ACCEPT=,defer-accept 
unset -v READ_BUFFER ; READ_BUFFER=,readbytes=1000000 
unset -v RELAY ; # RELAY=127.0.0.0:80 
unset -v RETRY_COUNT ; # RETRY_COUNT=,syncnt=1 
if [[ ${#RELAY} -ne 0 ]] ; then OX=tcp:${RELAY},keepalive,linger=60,nodelay${RETRY_COUNT} ; else OX=exec:./ox-proxy.sh,pipes ; fi 
if socat ${DIAGNOSTICS} -u open:/dev/null,readbytes=1 open:/dev/null,su=nobody ; then su_nobody=",su=nobody" ; else su_nobody= ; fi 
( dirname_re="(^.*/)[^/]+$" ; [[ "${BASH_SOURCE[0]}" =~ $dirname_re ]] && cd "${BASH_REMATCH[1]}" ; socat ${DIAGNOSTICS} -T 60 ssl-l:${PORT},pf=ip6,ipv6only=0,cert=${CERT},key=${KEY},cafile=${CA},verify=0,fork,keepalive,linger=60,nodelay${DEFER_ACCEPT}${READ_BUFFER},reuseaddr${su_nobody} ${OX} ) 
exit 0
